const a8connect = require('@autom8/js-a8-connect')
const a8instance = new a8connect({hostname : 'gateway'})

const invokeHandler= ()=>{
  const handler = {
    get: function(obj, prop){
      obj.call.push(prop)
      return new Proxy(obj,handler)
    }
  }

  const callFunction = (inputJson) => {
    const functionPath = callFunction.call.join('.')

    //reset call to blank
    callFunction.call = []

    console.log(`function path is ${functionPath}, the function inputs is ${JSON.stringify(inputJson)}`)

    return a8instance.invoke(functionPath, inputJson)
  }

  callFunction.call = []

  return new Proxy(callFunction, handler)
}


const a8 = invokeHandler()

module.exports = a8